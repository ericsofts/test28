<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CarManageController;
use App\Http\Controllers\CarModelController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('users')->group(function () {
    Route::get('/', [UserController::class, 'getAll'])->name('all_users');
    Route::get('/{user:id}/cars', [UserController::class, 'getUserCars'])->name('getUserCars');
});

Route::prefix('cars')->group(function () {
    Route::get('/', [CarController::class, 'getAll'])->name('all_cars');
    Route::get('/{car:id}', [CarController::class, 'getCar'])->name('getCar');
});

Route::prefix('brands')->group(function () {
    Route::get('/', [BrandController::class, 'getAll'])->name('all_brands');
});

Route::prefix('models')->group(function () {
    Route::get('/', [CarModelController::class, 'getAll'])->name('all_models');
});

Route::prefix('manage-cars')->group(function () {
    Route::post('/store/{brand:id}/{model:id}/{user:id?}', [CarManageController::class, 'store'])->name('store_car');
    Route::post('/update/{car:id}/{brand:id}/{model:id}/{user:id}', [CarManageController::class, 'update'])->name('update_car');
    Route::post('/delete/{car:id}', [CarManageController::class, 'delete'])->name('delete_car');
});
