<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => rand(1, 3),
            'brand_id' => rand(1, 5),
            'car_model_id' => rand(1, 5),
            'year' => Carbon::now(),
            'color' => fake()->colorName,
            'mileage' => rand(1000, 3000),
        ];
    }
}
