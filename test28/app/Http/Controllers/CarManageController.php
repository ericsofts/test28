<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCarRequest;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @OA\Tag(
 *     name="Manage Cars",
 *     description="Manage Cars"
 * )
 */
class CarManageController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/manage-cars/store/{brand:id}/{model:id}/{user:id}",
     *     summary="Store Car",
     *     tags={"Manage Cars"},
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="brand:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="model:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="user:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="year",
     *                     type="string"
     *                 ),
     *                 example={"year": "1999"},
     *                 @OA\Property(
     *                     property="mileage",
     *                     type="number"
     *                 ),
     *                 example={"mileage": "10000"},
     *                 @OA\Property(
     *                     property="color",
     *                     type="string"
     *                 ),
     *                 example={"color": "Yellow"},
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": {}}, summary="An result object."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     *
     * @param int $brandId
     * @param int $modelId
     * @param int $userId
     * @param AddCarRequest $request
     * @return JsonResponse
     */
    public function store(int $brandId, int $modelId, int $userId, AddCarRequest $request): JsonResponse
    {
        if (!User::where('id', '=', $userId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такого пользователя нет'
            ]);
        }

        if (!Brand::where('id', '=', $brandId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такой марки нет'
            ]);
        }

        if (!CarModel::where('id', '=', $modelId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такой модели нет'
            ]);
        }

        $car = Car::create([
            'user_id' => $userId,
            'brand_id' => $brandId,
            'car_model_id' => $modelId,
            'year' => Carbon::parse($request->input('year'))->format('Y'),
            'color' => $request->input('color'),
            'mileage' => $request->input('mileage'),
        ]);

        if ($car) {
            return response()->json([
                'success' => true,
                'message' => 'Автомобиль добавлен'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Что-то пошло не так'
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/manage-cars/update/{car:id}/{brand:id}/{model:id}/{user:id}",
     *     summary="Update Car",
     *     tags={"Manage Cars"},
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="car:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="brand:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="model:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="user:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="year",
     *                     type="string"
     *                 ),
     *                 example={"year": "1999"},
     *                 @OA\Property(
     *                     property="mileage",
     *                     type="number"
     *                 ),
     *                 example={"mileage": "10000"},
     *                 @OA\Property(
     *                     property="color",
     *                     type="string"
     *                 ),
     *                 example={"color": "Yellow"},
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": {}}, summary="An result object."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     *
     * @param int $carId
     * @param int $brandId
     * @param int $modelId
     * @param int $userId
     * @param AddCarRequest $request
     * @return JsonResponse
     */
    public function update(int $carId, int $brandId, int $modelId, int $userId, AddCarRequest $request): JsonResponse
    {
        if (!Car::where('id', '=', $carId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такого автомобиля нет'
            ]);
        }

        if (!User::where('id', '=', $userId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такого пользователя нет'
            ]);
        }

        if (!Brand::where('id', '=', $brandId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такой марки нет'
            ]);
        }

        if (!CarModel::where('id', '=', $modelId)->exists()) {
            return response()->json([
                'success' => false,
                'message' => 'Такой модели нет'
            ]);
        }

        $car = Car::create([
            'user_id' => $userId,
            'brand_id' => $brandId,
            'car_model_id' => $modelId,
            'year' => Carbon::parse($request->input('year'))->format('Y'),
            'color' => $request->input('color'),
            'mileage' => $request->input('mileage'),
        ]);

        if ($car) {
            return response()->json([
                'success' => true,
                'message' => 'Автомобиль добавлен'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Что-то пошло не так'
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/manage-cars/delete/{car:id}",
     *     summary="Delete Car",
     *     tags={"Manage Cars"},
     *     @OA\Parameter(
     *         description="Parameter with mutliple examples",
     *         in="path",
     *         name="car:id",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         @OA\Examples(example="integer", value="1", summary="An int value."),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": {}}, summary="An result object."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $car = Car::find($id);

        if (!$car) {
            return response()->json([
                'success' => false,
                'message' => 'Такого автомобиля нет'
            ]);
        }

        if ($car->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Автомобиль удален'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Что-то пошло не так'
        ]);
    }
}
