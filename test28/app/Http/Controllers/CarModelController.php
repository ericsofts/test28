<?php

namespace App\Http\Controllers;

use App\Http\Resources\BrandResource;
use App\Models\CarModel;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Tag(
 *     name="Models",
 *     description="Models"
 * )
 */
class CarModelController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/models",
     *     summary="Get all Models",
     *     tags={"Models"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true, "data": {}}, summary="An result object."),
     *         )
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $models = CarModel::all();

        return response()->json([
            'success' => true,
            'data' => BrandResource::collection($models)
        ]);
    }
}
