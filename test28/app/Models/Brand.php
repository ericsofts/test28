<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Brand
 *
 * @method static Builder find(int $id)
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder create(array $attributes = [])
 * @method static Builder update(array $values)
 * @method static Builder updateOrCreate(array $values, array $values = [])
 * @method static Builder firstOrCreate(array $values, array $values = [])
 */
class Brand extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];
}
